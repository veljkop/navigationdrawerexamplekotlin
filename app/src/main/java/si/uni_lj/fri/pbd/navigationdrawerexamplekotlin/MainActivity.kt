package si.uni_lj.fri.pbd.navigationdrawerexamplekotlin

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import si.uni_lj.fri.pbd.navigationdrawerexamplekotlin.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    var drawerLayout: DrawerLayout? = null
    var toggle: ActionBarDrawerToggle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        drawerLayout = binding.activityMain
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close)

        drawerLayout?.addDrawerListener(toggle!!)
        toggle?.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.nv.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.account -> {
                    Toast.makeText(this, "My Account", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.settings -> {
                    Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.mycart -> {
                    Toast.makeText(this, "My Cart", Toast.LENGTH_SHORT).show()
                    true
                }
                else -> false
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(toggle?.onOptionsItemSelected(item)!!)
            return true
        return super.onOptionsItemSelected(item)
    }
}